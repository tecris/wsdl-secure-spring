package spring.ws.soap;

import java.util.logging.Logger;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import spring.ws.soap.xsd.EventType;
import spring.ws.soap.xsd.ScoreboardRequest;
import spring.ws.soap.xsd.ScoreboardResponse;

@Endpoint
public class ScoreboardEndpoint {

    private static final Logger LOGGER = Logger.getLogger(ScoreboardEndpoint.class.getName());

    private static final String NAMESPACE_URI = "http://github.com/tecris/spring.ws.soap";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "ScoreboardRequest")
    @ResponsePayload
    public ScoreboardResponse sendScoreboard(@RequestPayload ScoreboardRequest request) {
        EventType event = request.getEvent();
        String out = event.getCompetition() + " -> " + event.getHomeTeamName() + " - " + event.getVisitorTeamName();
        LOGGER.info(out);
        ScoreboardResponse scoreboardResponse = new ScoreboardResponse();
        scoreboardResponse.setEvent(event);
        return scoreboardResponse;
    }
}
