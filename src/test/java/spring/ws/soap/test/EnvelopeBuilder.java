package spring.ws.soap.test;

import java.io.StringWriter;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import spring.ws.soap.xsd.EventType;

public class EnvelopeBuilder {

    public static String buildSoapEnvelope(EventType event) throws Exception {

        UsernameTokenPasswordDigest tokenDigest = new UsernameTokenPasswordDigest("nimbo");

        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

        SOAPHeader soapHeader = soapEnvelope.getHeader();
        SOAPHeaderElement headerElement =
                        soapHeader.addHeaderElement(
                                        soapEnvelope.createName(
                                            "Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"));

        SOAPElement usernameTokenSOAPElement = headerElement.addChildElement("UsernameToken", "wsse");
        usernameTokenSOAPElement.addAttribute(
            soapEnvelope.createName("id", "wsu",
                "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"), "UsernameToken-1");

        usernameTokenSOAPElement.addChildElement("Username", "wsse").setTextContent("cloud");

        SOAPElement passwordSOAPElement = usernameTokenSOAPElement
                        .addChildElement("Password", "wsse");
        passwordSOAPElement
                        .addAttribute(
                            soapEnvelope.createName("Type"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest");
        passwordSOAPElement.setTextContent(tokenDigest.getPasswordDigest());

        SOAPElement nonceSOAPElement = usernameTokenSOAPElement
                        .addChildElement("Nonce", "wsse");
        nonceSOAPElement
                        .addAttribute(
                            soapEnvelope.createName("EncodingType"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary");
        nonceSOAPElement.setTextContent(tokenDigest.getNonce());

        usernameTokenSOAPElement.addChildElement("Created", "wsu").setTextContent(tokenDigest.getTimeStamp());

        SOAPBody soapBody = soapEnvelope.getBody();

        SOAPBodyElement bodyElement =
                        soapBody.addBodyElement(soapEnvelope.createName("ScoreboardRequest", "", "http://github.com/tecris/spring.ws.soap"));
        SOAPElement eventElement = bodyElement.addChildElement(soapEnvelope.createName("Event"));
        SOAPElement competitionElement = eventElement.addChildElement("competition");
        competitionElement.setTextContent(event.getCompetition());

        SOAPElement homeTeamNameElement = eventElement.addChildElement("homeTeamName");
        homeTeamNameElement.setTextContent(event.getHomeTeamName());

        SOAPElement visitorTeamNameElement = eventElement.addChildElement("visitorTeamName");
        visitorTeamNameElement.setTextContent(event.getVisitorTeamName());

        Source source = soapPart.getContent();
        Transformer transformer = TransformerFactory.newInstance()
                        .newTransformer();

        StreamResult xmlOutput = new StreamResult(new StringWriter());
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.ENCODING, "no");
        transformer.transform(source, xmlOutput);

        return xmlOutput.getWriter().toString();
    }
}
