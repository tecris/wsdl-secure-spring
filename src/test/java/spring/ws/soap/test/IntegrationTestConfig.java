package spring.ws.soap.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.ws.config.annotation.EnableWs;

@Configuration
@ComponentScan(
                basePackages = {
                        "spring.ws.soap" })
@EnableWs
public class IntegrationTestConfig extends WebMvcConfigurationSupport {

}
