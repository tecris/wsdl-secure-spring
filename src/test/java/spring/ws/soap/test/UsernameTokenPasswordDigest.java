package spring.ws.soap.test;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class UsernameTokenPasswordDigest {

    private static final String ZED = "Z";
    private static final String UTF_8 = "UTF-8";

    private String passwordDigest;
    private String nonce;
    private String timeStamp;

    public UsernameTokenPasswordDigest(String password) {

        this.nonce = this.buildNonce();
        this.timeStamp = this.buildTimestamp();
        this.passwordDigest = this.buildPasswordDigest(nonce, timeStamp, password);
    }

    private String buildNonce() {

        String dateTimeString = Long.toString(new Date().getTime());
        byte[] nonceByte = dateTimeString.getBytes();
        return new String(Base64.encodeBase64(nonceByte));
    }

    private String buildTimestamp() {
        String timeStampString;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        timeStampString = DatatypeConverter.printDateTime(c);
        timeStampString = timeStampString.substring(0, 19) + ZED;
        return timeStampString;
    }

    private String buildPasswordDigest(String nonce, String timestamp, String password) {

        String passwordDigest;
        ByteBuffer buf = ByteBuffer.allocate(1000);
        buf.put(Base64.decodeBase64(nonce));
        try {
            buf.put(timestamp.getBytes(UTF_8));
            buf.put(password.getBytes(UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] toHash = new byte[buf.position()];
        buf.rewind();
        buf.get(toHash);
        byte[] hash = DigestUtils.sha1(toHash);
        passwordDigest = Base64.encodeBase64String(hash);
        return passwordDigest;
    }

    public String getPasswordDigest() {
        return passwordDigest;
    }

    public String getNonce() {
        return nonce;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

}
