package spring.ws.soap;

import static org.junit.Assert.assertEquals;

import javax.xml.soap.SOAPException;
import javax.xml.ws.BindingProvider;

import org.junit.Test;

public class ScoreboardEndpointIT {

    private final static String END_POINT_URL = "http://localhost:8080/ws";

    @Test
    public void test() throws SOAPException {

        String expectedCompetitionName = "La Tabon";
        ScoreboardRequest scoreboardRequest = new ScoreboardRequest();
        scoreboardRequest.setEvent(this.buildEventType(expectedCompetitionName));

        ScoreboardPortService scoreboardPortService = new ScoreboardPortService();

        ScoreboardPort scoreboardPort = scoreboardPortService.getScoreboardPortSoap11();
        ((BindingProvider) scoreboardPort).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                END_POINT_URL);

        ScoreboardResponse scoreboardResponse = scoreboardPort.scoreboard(scoreboardRequest);
        assertEquals(scoreboardResponse.getEvent().getCompetition(), expectedCompetitionName);
    }

    private EventType buildEventType(String competition) {
        EventType eventType = new EventType();
        eventType.setCompetition(competition);
        eventType.setHomeTeamName("Shakhtar");
        eventType.setVisitorTeamName("Porto");
        return eventType;
    }

}
