package spring.ws.soap;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.ws.test.server.RequestCreators;
import org.springframework.xml.transform.StringSource;

import spring.ws.soap.test.EnvelopeBuilder;
import spring.ws.soap.test.IntegrationTestConfig;
import spring.ws.soap.xsd.EventType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { IntegrationTestConfig.class })
@WebAppConfiguration
public class ScoreboardEndpointTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockWebServiceClient mockClient;

    @Before
    public void createClient() throws IOException, URISyntaxException {

        mockClient = MockWebServiceClient.createClient(applicationContext);
        MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test() throws Exception {
        this.sendUpdate(this.buildEventType());
    }

    private EventType buildEventType() {
        EventType eventType = new EventType();
        eventType.setCompetition("competition");
        eventType.setHomeTeamName("Shakhtar");
        eventType.setVisitorTeamName("Porto");
        return eventType;
    }

    private void sendUpdate(EventType event) throws Exception {

        String s = EnvelopeBuilder.buildSoapEnvelope(event);

        mockClient.sendRequest(
                RequestCreators.withSoapEnvelope(new StringSource(EnvelopeBuilder.buildSoapEnvelope(event))));
    }
}
