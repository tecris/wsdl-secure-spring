# Secure Web Services with Spring

Boilerplate Spring Secure Web Services
    
## How to run application
### With Spring Boot
   ```sh
     $ mvn clean package
     $ java -jar target/ROOT.war
   ```

### With Docker
   ```sh
      $ docker run -d --name wildfly-soap10 -p 8080:8080 -p 9990:9990 casadocker/alpine-wildfly-soap:10.0.0    # start wildfly container
      $ mvn wildfly:deploy                                                                              # deploy application
      $ mvn wildfly:undeploy                                                                            # undeploy application
   ```
#### Integration tests (broken 15-Feb-2016)
 * `$ mvn clean verify -Pcontinuous-delivery -Dmaven.buildNumber.doCheck=false`

### Test deployment:
   * `$ ./postRequest.sh`

### WSDL
   * `http://localhost:8080/ws/scoreboard.wsdl`
